<!DOCTYPE html>
<html> 


<head> 
	<!-- Charset utf8 -->
	<meta charset="UTF-8">

	<!-- Título e meta description --> 
	<title><?php echo $seo_title; ?></title>
	<meta name="description" content="<?php echo $seo_description; ?>"> 
	<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1, minimum-scale=1" />
	
	<?php
	
	// Se as Open Graph Tag foram autorizadas
	if ( $OpenGraphTag["ativo"] == true ) {
		
		?>
		<!-- Open Graph tag -->
		<meta property="og:type" content="<?php echo $OpenGraphTag["type"]; ?>">
		<meta property="og:site_name" content="<?php echo $GLOBALS["nome_padrao"]; ?>">
		<meta property="og:title" content="<?php echo $seo_title; ?>">
		<meta property="og:description" content="<?php echo $seo_description; ?>">
		<meta property="og:url" content="<?php echo $OpenGraphTag["url"]; ?>">
		<meta property="og:image" content="<?php echo $OpenGraphTag["img"]; ?>">
		<?php
		
	}
	
	?>
	
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo $GLOBALS["urlbase_atual"]; ?>_imagens/layout/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo $GLOBALS["urlbase_atual"]; ?>_imagens/layout/favicon.ico" type="image/x-icon">
	

	<!-- Arquivos CSS --> 
	<link href="<?php echo $GLOBALS["urlbase_atual"]; ?>_css/layout.css?v=1.8" rel="stylesheet" type="text/css">
	<link href="<?php echo $GLOBALS["urlbase_atual"]; ?>_css/jquery.fullPage.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $GLOBALS["urlbase_atual"]; ?>_css/bootstrap.css?v=1" rel="stylesheet" type="text/css">
	<link href="<?php echo $GLOBALS["urlbase_atual"]; ?>_css/fontawesome-all.css" rel="stylesheet" type="text/css">

</head> 

<body>

	<div id="website">

		<header>

			<!-- Logo -->
			<a href="<?php echo $GLOBALS['urlbase_atual']; ?>" id="logo">
				<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/logo.png?v=1" alt="Logo">
			</a>

			<a class="menu-toggle">
				<span class="icon-menu-els">
					<span class="menu-bar"></span>
					<span class="menu-bar"></span>
					<span class="menu-bar"></span>
				</span>
			</a>

			<nav id="menu">

				<ul>
					<li data-menuanchor="carnaval"><a href="<?php echo $GLOBALS['urlbase_atual']; ?>#carnaval">
						<span class="menu-pager"></span>
						<span>Carnaval</span>
					</a></li>
					<li data-menuanchor="brasil"><a href="<?php echo $GLOBALS['urlbase_atual']; ?>#brasil">
						<span class="menu-pager"></span>
						<span>Brasil</span>
					</a></li>
					<li data-menuanchor="bahia"><a href="<?php echo $GLOBALS['urlbase_atual']; ?>#bahia">
						<span class="menu-pager"></span>
						<span>Bahia</span>
					</a></li>
					<li data-menuanchor="txai"><a href="<?php echo $GLOBALS['urlbase_atual']; ?>#txai">
						<span class="menu-pager"></span>
						<span>Txai</span>
					</a></li>
					<li data-menuanchor="roteiro"><a href="<?php echo $GLOBALS['urlbase_atual']; ?>#roteiro">
						<span class="menu-pager"></span>
						<span>Roteiro</span>
					</a></li>
				</ul>

			</nav>

			<a href="#" class="chateaux"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/chateaux.png" alt="Chateaux"></a>

			<div class="social-text">FOLLOW US</div>

			<div class="social">

				<a href="#" target="_blank">
					<i class="fab fa-facebook-f" aria-hidden="true"></i>
				</a>

				<a href="#" target="_blank">
					<i class="fa fa-globe" aria-hidden="true"></i>
				</a>

				<a href="#" target="_blank">
					<i class="fab fa-instagram" aria-hidden="true"></i>
				</a>

				<a href="#" target="_blank">
					<i class="fab fa-youtube" aria-hidden="true"></i>
				</a>

			</div>

		</header>
