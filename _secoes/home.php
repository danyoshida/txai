<?php

/* ----------------------------------------------------------------
** Home
** --------------------------------------------------------------*/

if(count($_POST) > 0){

	$retorno = emails::mensagemContato($_POST);

	if($retorno["resultado"]){

		//Define a mensagem de sucesso
		$msg = "Formulário enviado com sucesso!";

	}

}


/* SEO 
--------------------------------------------------------------*/
$seo_title = $GLOBALS["nome_padrao"];
$seo_description = "";



/* Open Graph Tag
--------------------------------------------------------------*/
$OpenGraphTag["ativo"] = false; 
$OpenGraphTag["type"] = ""; 
$OpenGraphTag["url"] = ""; 
$OpenGraphTag["img"] = ""; 



/* Cabeçalho do site
--------------------------------------------------------------*/
include $localbase . "_modulos/header.php"; 



/* Conteúdo da página
--------------------------------------------------------------*/
?>
<div id="fullpage">

	<section class="section section1" data-anchor="carnaval" data-speed="0.4">

		<div class="text-block">

			<h1>Carnival</h1>
			<p>the biggest celebration <span>in Brazil</span></p>
			<p>the largest street party <span>in world</span></p>

		</div>

		<div class="scroll-button">

			<a href="#brasil"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/scroll-down.png" alt="Ir para a seção abaixo"></a>

		</div>

	</section>

	<section class="section section2" data-anchor="brasil" data-speed="0.4">

		<div class="container">

			<div class="row">

				<div class="col-md-5" data-aos="fade-up">
						
					<h2>O Brasil guarda dentro de si um mundo de culturas.</h2>

					<p>De Norte a Sul, o Brasil expressa sua cultura rica e diversificada. A miscigenada herança indígena, europeia e africana se apresenta em práticas religiosas, sabores e festas únicas. Uma experiência imperdível.</p>

					<p>Localizado na América do Sul, o maior país do continente, abriga 5 grandes regiões. Entre elas, o Nordeste abriga o Estado da Bahia, onde está a cidade de Itacaré.</p>

				</div>

				<div class="col-md-7 video">

					<video id="video"><source src="" type="video/mp4"></video>

					<div class="play">
						<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/play.png" alt="Play">
					</div>

				</div>

			</div>

		</div>

		<div class="scroll-button">

			<a href="#bahia"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/scroll-down.png" alt="Ir para a seção abaixo"></a>

		</div>

	</section>

	<section class="section section3" data-anchor="bahia" data-speed="0.4">

		<div class="text-block">

			<h2>Bahia: o berço histórico e cultural brasileiro.</h2>
			<p>A Bahia foi a primeira a receber as embarcações portuguesas em território brasileiro. De lá, todo o país recebeu traços alegres e festeiros. Repleta de belezas naturais, construções históricas e festas folclóricas, o deslumbrante território baiano precisa estar no seu roteiro de viagem.</p>

		</div>

		<div class="scroll-button">

			<a href="#txai"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/scroll-down.png" alt="Ir para a seção abaixo"></a>

		</div>

	</section>


	<section class="section section4" data-anchor="txai" data-speed="0.4">

		<div class="slide bg1">
			<p>Entre a icônica capital Salvador e a histórica cidade de Porto Seguro, na Bahia, está a linda Itacaré. Um local estratégico para conhecer o litoral baiano.</p>
		</div>

		<div class="slide bg2">
			<p>Com uma costa litorânea sem igual, Itacaré abriga o Txai Resort, dentro de uma antiga fazenda de coco e cacau. </p>
		</div>

		<div class="slide bg3">
			<p>No Txai Resort Itacaré, cada detalhe foi pensado para você viver o luxo da simplicidade de forma aconchegante e integrada a natureza.</p>
		</div>

		<div class="slide bg4">
			<p>Os hóspedes ficam acomodados em bangalôs a poucos passos do mar ou no topo dos morros. Em ambos os casos, a vista paradisíaca é garantida.</p>
		</div>

		<div class="slide bg5">
			<p>Os sabores da Bahia se revelam em ingredientes locais preparados com técnicas europeias. E o jantar pode ser preparado de forma exclusiva.</p>
		</div>

		<div class="slide bg6">
			<p>O SPA Shamash, localizado no Txai, oferece massagens personalizadas para o seu biotipo. Para isso, conta com profissionais especializados nas mais diversas técnicas.</p>
		</div>

		<div class="scroll-button">

			<a href="#roteiro"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/scroll-down.png" alt="Ir para a seção abaixo"></a>

		</div>

	</section>

	<section class="section section5" data-anchor="roteiro" data-speed="0.4">

		<div class="container">

			<div class="row">

				<div class="col-md-4 text-center col-md-offset-1 col-sm-offset-1">

					<h3>Preencha o formulário 
					<br />e veja roteiros para conhecer 
					<br />o litoral da Bahia/BR.</h3>

					<form method="post">

						<?php

						if(isset($msg)){

							?><p class="alert alert-success"><?php echo $msg; ?></p><?php

						}

						//Se ocorreu algum erro no envio do formulário
						if(isset($retorno["resultado"]) and $retorno["resultado"] == false){

							?><div class="alert alert-danger">
								<ul><?php echo $retorno["erros"]; ?></ul>
							</div><?php

						}

						?>

						<label>NOME:</label>
						<input type="text" name="cmp_nome" required>

						<label>E-MAIL:</label>
						<input type="email" name="cmp_email" required>

						<label>TELEFONE:</label>
						<input type="text" name="cmp_telefone" required>

						<label>OBSERVAÇÃO:</label>
						<textarea name="cmp_observacao"></textarea>

						<input type="checkbox" name="cmp_newsletter" id="cmp_newsletter" value="1">
						<label for="cmp_newsletter" class="check">Desejo receber as novidade do TXAI</label>

						<a href="#" class="button-sm">FAZER DOWNLOAD</a>

						<input type="submit" value="Clique aqui e faça sua reserva no Txai!">

					</form>

				</div>

				<div class="col-md-6">

					<div class="col-md-12 text-center">
						<h2>Com tanta diversidade, conhecer os melhores lugares da Bahia demanda alguns dias. Conheça o roteiro que preparamos para você com dicas de turismo pela costa baiana.</h2>

						<h3>Confira também um programa de 
						<br />hospedagem no Txai Resort.</h3>

						<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/foto.jpg" alt="Txai Resort">
					</div>

					<div class="col-md-12">

						<div class="col-md-4 col-sm-4 col-xs-12">
							<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/award1.jpg" alt="" class="img-responsive">
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
							<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/award2.jpg" alt="" class="img-responsive">
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
							<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/award3.jpg" alt="" class="img-responsive">
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-2 col-sm-offset-2">
							<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/award4.jpg" alt="" class="img-responsive">
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
							<img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/award5.jpg" alt="" class="img-responsive">
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 col-md-offset-4 col-sm-offset-4">
							<a href="#"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/focusnetworks.jpg" alt="" class="img-responsive"></a>
						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="scroll-button">

			<a href="#carnaval"><img src="<?php echo $GLOBALS['urlbase_atual']; ?>_imagens/layout/scroll-up.png" alt="Voltar para o topo"></a>

		</div>

	</section>

</div>

<?php



/* Rodapé do site
--------------------------------------------------------------*/
include $localbase . "_modulos/footer.php"; 

?>