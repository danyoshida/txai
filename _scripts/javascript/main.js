$(document).ready(function(){

	//Ativa os placeholder para versões de navegadores antigos  
    $('input, textarea').placeholder();

    var scroll_overflow = true;

    if($(window).width() < 991) scroll_overflow = false;

    $('#fullpage').fullpage({
    	anchors:['carnaval', 'brasil', 'bahia', 'txai', 'roteiro'],
    	//Navigation
    	menu: '#menu',
    	css3: true,
    	scrollOverflow:scroll_overflow,
        responsiveWidth: 992,
    	onLeave: function(index, nextIndex, direction){}
    });

    $(".video").click(function(){

        var video = $("#video")[0];
        
        video.paused ? video.play() : video.pause();

        $(this).find(".play").toggleClass("hide");

    });

    $(".menu-toggle").click(function(){
        
        $("html").toggleClass("show");

    });

    $("nav a").click(function(){

        $("html").toggleClass("show");

    });

});