<?php


class emails {
	
	
	/* Envia uma determinada mensagem
	--------------------------------------------------------------*/
	static function enviarMensagem($destinatario, $replyto, $assunto, $mensagem) {
		
		// Decodifica o assunto e mensagem para caracteres normais "não html"
		$assunto = trataStrings::recuperaCampo($assunto); 
		$mensagem = trataStrings::recuperaCampo($mensagem); 
		
		//Ajusta a codificação do assunto para UTF-8
		$assunto = '=?UTF-8?B?'.base64_encode($assunto).'?=';

		if (PATH_SEPARATOR ==":") {
			$quebra = "\r\n";
		} else {
			$quebra = "\n";
		}
		$headers = "MIME-Version: 1.1".$quebra;
		$headers .= "Content-type: text/html; charset=utf-8".$quebra;
		$headers .= "From: ".$GLOBALS["email_padrao"].$quebra; //E-mail do remetente
		$headers .= "Return-Path: ".$GLOBALS["email_padrao"].$quebra; //E-mail do remetente

		mail($destinatario, $assunto, $mensagem, $headers, $GLOBALS["email_padrao"]);
		
	}
	
	
	
	/* Mensagem de contato
	--------------------------------------------------------------*/
	static function mensagemContato($mensagem) {
		
		
		/* Inicia a validação
		--------------------------------------------------------------*/
		
		// Se o nome estiver vazio
		if (validaFormulario::preenchido($mensagem["cmp_nome"])) $retorno["erros"] .= "<li>O nome deve ser informado</li>"; 
		
		// Se o e-mail for inválido
		if ( validaFormulario::email($mensagem["cmp_email"])) $retorno["erros"] .= "<li>E-mail inválido</li>"; 
		
		// Se o e-mail estiver vazio
		if ( validaFormulario::preenchido($mensagem["cmp_telefone"])) $retorno["erros"] .= "<li>O telefone deve ser informado</li>"; 
		
		
		/* Envia a mensagem ou retorna o erro
		--------------------------------------------------------------*/
		
		// Se nenhum erro ocorreu na validação 
		if ( !isset($retorno["erros"]) ) {
			
			if($mensagem["cmp_newsletter"] == 1) $newsletter = "sim";
			else $newsletter = "não";

			// Monta a mensagem a ser enviada
			$msgMontada = "<p>Formulário de contato</p><br />";
			$msgMontada .= "<p>Nome: " . $mensagem["cmp_nome"] . "</p> \n";
			$msgMontada .= "<p>Telefone: " . $mensagem["cmp_telefone"] . "</p> \n";
			$msgMontada .= "<p>E-Mail: " . $mensagem["cmp_email"] . "</p> \n";
			$msgMontada .= "<p>Observação: " . $mensagem["cmp_observacao"] . "</p> \n";
			$msgMontada .= "<p>Desejo receber as novidade do TXAI: " . $newsletter . "</p> \n";
			
			// Envia a mensagem montada
			self::enviarMensagem($GLOBALS["email_padrao"], $mensagem["email"], "Contato pelo site", $msgMontada); 
			
			
			// Finaliza a montagem do array de retorno
			$retorno["resultado"] = true;
			
			// Retorna o array de retorno
			return $retorno; 
			
		}
		
		// Caso algum erro na validação tenha ocorrido
		else {
			
			// Finaliza a montagem do array de retorno
			$retorno["resultado"] = false;
			
			// Retorna o array de retorno
			return $retorno; 
			
		}
		
	}

}



?>